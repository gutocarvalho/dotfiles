# loading promptline shell
if [ -f ~/.shell_prompt.sh ]; then
  source ~/.shell_prompt.sh
fi

# Autocorrect typos in path names when using `cd`
shopt -s cdspell;

# Add tab completion for SSH hostnames based on ~/.ssh/config, ignoring wildcards
[ -e "$HOME/.ssh/config" ] && complete -o "default" -o "nospace" -W "$(grep "^Host" ~/.ssh/config | grep -v "[?*]" | cut -d " " -f2- | tr ' ' '\n')" scp sftp ssh;


# function to extract files
extract () {
        if [ -f $1 ] ; then
          case $1 in
            *.tar.bz2)   tar xjf $1     ;;
            *.tar.gz)    tar xzf $1     ;;
            *.bz2)       bunzip2 $1     ;;
            *.rar)       unrar e $1     ;;
            *.gz)        gunzip $1      ;;
            *.tar)       tar xf $1      ;;
            *.tbz2)      tar xjf $1     ;;
            *.tgz)       tar xzf $1     ;;
            *.zip)       unzip $1       ;;
            *.Z)         uncompress $1  ;;
            *.7z)        7z x $1        ;;
            *)     echo "'$1' cannot be extracted via extract()" ;;
             esac
         else
             echo "'$1' is not a valid file"
         fi
    }

# Always list directory contents upon 'cd'
cd() { builtin cd "$@"; ll; }

# Moves a file to the MacOS trash
trash () { command mv "$@" ~/.Trash ; }

# history with grep
h() {
    if [ -z "$1" ]
    then
        history
    else
        history | grep "$@"
    fi
}

# set current tab name to a custom string
tabname() {
  printf "\e]1;$1\a"
}

# set window name to a custom string
winname() {
  printf "\e]2;$1\a"
}

# showa: to remind yourself of an alias (given some part of it)
showa() { alias|grep $1 |less; }

alias editBash='atom ~/.bash_profile'

alias cp='cp -iv'                           # Preferred 'cp' implementation
alias mv='mv -iv'                           # Preferred 'mv' implementation
alias rm='rm -iv'                           # Preferred 'mv' implementation
alias mkdir='mkdir -pv'                     # Preferred 'mkdir' implementation
alias df='df -h'
alias which='type -all'                     # which:        Find executables

alias ls='ls -G'
alias ll='ls -FGlAhp'                       # Preferred 'ls' implementation
alias la='ls -a'                            # Preferred 'ls' implementation

#   lr:  Full Recursive Directory Listing
alias lr='ls -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'' | less'

alias c='clear'                             # c:            Clear terminal display
alias f='open -a Finder ./'                 # f:            Opens current directory in MacOS Finder
alias path='echo -e ${PATH//:/\\n}'         # path:         Echo all executable Paths

alias less='less -FSRXc'                    # Preferred 'less' implementation
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'

alias qfind="find . -name "                 # qfind:    Quickly search for file
ff () { /usr/bin/find . -name "$@" ; }      # ff:       Find file under the current directory
ffs () { /usr/bin/find . -name "$@"'*' ; }  # ffs:      Find file whose name starts with a given string
ffe () { /usr/bin/find . -name '*'"$@" ; }  # ffe:      Find file whose name ends with a given string
spotlight () { mdfind "kMDItemDisplayName == '$@'wc"; } # Spotlight cli search
alias cleanupDS="find . -type f -name '*.DS_Store' -ls -delete"

alias finderShowHidden='defaults write com.apple.finder AppleShowAllFiles TRUE && killall Finder'
alias finderHideHidden='defaults write com.apple.finder AppleShowAllFiles FALSE && killall Finder'

alias memHogsTop='top -l 1 -o rsize | head -20'
alias memHogsPs='ps wwaxm -o pid,stat,vsize,rss,time,command | head -10'
alias cpu_hogs='ps wwaxr -o pid,stat,%cpu,time,command | head -10'
alias ttop="top -R -F -s 10 -o rsize"
alias topForever='top -l 9999999 -s 10 -o cpu' # ttop:  Recommended 'top' invocation to minimize resources

#   my_ps: List processes owned by my user:
my_ps() { ps $@ -u $USER -o pid,%cpu,%mem,start,time,bsdtime,command ; }

alias netCons='lsof -i'                             # netCons:      Show all open TCP/IP sockets
alias flushDNS='dscacheutil -flushcache'            # flushDNS:     Flush out the DNS Cache
alias lsock='sudo /usr/sbin/lsof -i -P'             # lsock:        Display open sockets
alias lsockU='sudo /usr/sbin/lsof -nP | grep UDP'   # lsockU:       Display only open UDP sockets
alias lsockT='sudo /usr/sbin/lsof -nP | grep TCP'   # lsockT:       Display only open TCP sockets
alias openPorts='sudo lsof -i | grep LISTEN'        # openPorts:    All listening connections

zipf () { zip -r "$1".zip "$1" ; }          # zipf:         To create a ZIP archive of a folder
alias numFiles='echo $(ls -1 | wc -l)'      # numFiles:     Count of non-hidden files in current dir
alias make1mb='mkfile 1m ./1MB.dat'         # make1mb:      Creates a file of 1mb size (all zeros)
alias make5mb='mkfile 5m ./5MB.dat'         # make5mb:      Creates a file of 5mb size (all zeros)
alias make10mb='mkfile 10m ./10MB.dat'      # make10mb:     Creates a file of 10mb size (all zeros)
alias make1gb='mkfile 1g ./10GB.dat'        # make1gb:     Creates a file of 10gb size (all zeros)

alias newpwd='openssl rand -base64 18 | tee >(pbcopy) ; echo' # Generate a random password and copy to clipboard

newmac() {
  NEWMAC=$(openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//')
  echo "Changing MAC " $(ifconfig $1 | grep ether)
  sudo ifconfig $1 ether $NEWMAC
  echo "New MAC is:" $(ifconfig $1 | grep ether)
}

newhost() {
  NEWHOST=$(sed `perl -e "print int rand(99999)"`"q;d" /usr/share/dict/words)
  echo "Changing hostname: " $(hostname)
  sudo hostname $NEWHOST
  echo "New hostname is:" $(hostname)
}

hnp() {
  data=$(date +%Y%m%d%n)
  hugo new post/$data-$1.md
}

reloadrc() {
  source ~/.bash_profile
}

macdown() {
    "$(mdfind kMDItemCFBundleIdentifier=com.uranusjr.macdown | head -n1)/Contents/SharedSupport/bin/macdown" $@
}

alias finderShowHidden='defaults write com.apple.finder ShowAllFiles TRUE && killall Finder'
alias finderHideHidden='defaults write com.apple.finder ShowAllFiles FALSE && killall Finder'
alias cleanupDS="find . -type f -name '*.DS_Store' -ls -delete"
alias screensaverDesktop='/System/Library/Frameworks/ScreenSaver.framework/Resources/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine -background'

# Docker Aliases
alias dps='docker ps'
alias dli='docker images'

#deployblog() {
#  rsync -avz -e "ssh -o StrictHostKeyChecking=no -p port" --stats --progress /diretorio/origem/* user@domain:/diretorio/destino/
#}

#   cleanupLS:  Clean up LaunchServices to remove duplicates in the "Open With" menu
alias cleanupLS="/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder"

# loading bash_completion
if [ -f $(brew --prefix)/etc/bash_completion ]; then
   . $(brew --prefix)/etc/bash_completion
fi

# loading bashrc if exists
if [ -f ~/.bashrc ]; then
    source ~/.bashrc
fi

# loading profile if exists
if [ -f ~/.profile ]; then
    source ~/.profile
fi

# history config
export HISTTIMEFORMAT="%F %T "
export HISTFILESIZE=3000
export HISTSIZE=1000
export HISTCONTROL=ignoredups:ignorespace

# editor defaults
export EDITOR=/usr/bin/vim

# blocksized default
export BLOCKSIZE=1k

# path config
export PATH="/usr/local/sbin:$PATH"  # Add brew binaries PATH
export PATH="$PATH:/usr/local/packer" # Add packer binaries to PATH
export PATH="$PATH:/opt/puppetlabs/bin" # Add packer binaries to PATH

# loading atlas token
if [ -f ~/.atlas_token ]; then
    source ~/.atlas_token
fi

# loading deploy aliases
if [ -f ~/.deploy_sites ]; then
    source ~/.deploy_sites
fi

# vagrant config
export VAGRANT_HOME="~/storage/vagrant.d"
export VAGRANT_FORCE_COLOR=true

# loading rvm
if [ -f ~/.rvm/scripts/rvm ]; then
    source ~/.rvm/scripts/rvm
fi

## refs
## https://natelandau.com/my-mac-osx-bash_profile/
## https://github.com/mathiasbynens/dotfiles
